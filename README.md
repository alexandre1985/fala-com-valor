# Livro Falar com Valor

## Contribuir para este livro

Você pode contribuir neste livro. Mande-me para o meu endereço de
email um *git patch* com a sua contribuição. Outro método, é abrir um
*Merge Request* aqui no Gitlab, mas não garanto que irei estar atento
aos *Merge Requests* daqui.

## Licença

A licença permite a quem possui este livro, de copiar, imprimir,
modificar, vender. É **legal** fazê-lo. Uma leitura da licença (está,
em Inglês, no último capítulo do livro) é altamente aconselhável. O
nome da licença é "GNU Free Documentation License v1.3".

## Modificar o livro

Pode modificar o livro, desde que siga as condições impostas pela
licença do livro. Para saber mais leia o capítulo da licença que está
no livro.
