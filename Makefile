all: pdf info epub

pdf: falar-com-valor.texi
	texi2any --Xopt -t --Xopt @bsixpaper --Xopt --clean --pdf falar-com-valor.texi

pdf-a4: falar-com-valor.texi
	texi2any --Xopt -t --Xopt @afourpaper --Xopt --clean --pdf --output=falar-com-valor-a4.pdf falar-com-valor.texi

info: falar-com-valor.texi
	makeinfo falar-com-valor.texi

epub: falar-com-valor.texi
	texi2any --epub3 falar-com-valor.texi

booklet: falar-com-valor.pdf
	pdfbook2 falar-com-valor.pdf

booklet-a4: falar-com-valor-a4.pdf
	pdfbook2 falar-com-valor-a4.pdf

preview: falar-com-valor.texi
	rm -f falar-com-valor-temp.pdf falar-com-valor-amostra.pdf
	stapler sel falar-com-valor.pdf 1-18 falar-com-valor-temp.pdf
	stapler del falar-com-valor-temp.pdf 2 falar-com-valor-amostra.pdf
	rm falar-com-valor-temp.pdf

clean:
	rm -f falar-com-valor.info falar-com-valor.pdf falar-com-valor-a4.pdf falar-com-valor-book.pdf falar-com-valor.epub falar-com-valor-amostra.pdf
